## [2.0.5](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v2.0.4...v2.0.5) (2021-02-09)


### Bug Fixes

* Fix Issue 1 ([14a0edb](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/14a0edb))

## [2.0.4](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v2.0.3...v2.0.4) (2020-03-31)


### Bug Fixes

* Update to change STDIN detection ([dee3562](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/dee3562))

## [2.0.3](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v2.0.2...v2.0.3) (2020-03-26)


### Bug Fixes

* Update try except issue ([bbffb8c](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/bbffb8c))

## [2.0.2](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v2.0.1...v2.0.2) (2020-03-23)


### Bug Fixes

* Update log initialization ([536a11a](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/536a11a))

## [2.0.1](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v2.0.0...v2.0.1) (2020-01-23)


### Bug Fixes

* Correct flow error ([6721ed3](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/6721ed3))
* Re-add package index processing ([1336b4d](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/1336b4d))

# [2.0.0](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.5...v2.0.0) (2020-01-21)


### Bug Fixes

* Add pyyaml module requirement ([c4702ab](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/c4702ab))


### Code Refactoring

* Update module to use YAML config file ([af927c4](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/af927c4))


### BREAKING CHANGES

* Config file now uses a YAML formatted file.

    Before (list of packages):

    tox
    mypy

    After (yaml format):

    logging:
      version: 1
      formatters:
        simple:
          format: '[%(levelname)s]: %(message)s'
      handlers:
        console1:
          class: logging.StreamHandler
          level: ERROR
          formatter: simple
          stream: ext://sys.stderr
        console2:
          class: logging.StreamHandler
          level: DEBUG
          formatter: simple
          stream: ext://sys.stdout
    root:
      level: INFO
      stream: ext://sys.stdout
      handlers: [console1, console2]
    threads: 5
    packages:
      - tox
      - mypy
    blacklist:
      - pyyaml

## [1.0.5](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.4...v1.0.5) (2020-01-02)


### Bug Fixes

* update logging ([ef495d9](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/ef495d9))
* update logging ([aaad366](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/aaad366))

## [1.0.4](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.3...v1.0.4) (2019-12-31)


### Bug Fixes

* update help ([d4d2faf](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/d4d2faf))

## [1.0.3](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.2...v1.0.3) (2019-12-30)


### Bug Fixes

* update behavior of config file option ([82d87be](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/82d87be))

## [1.0.2](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.1...v1.0.2) (2019-12-27)


### Bug Fixes

* add python 3.6 or greater check for install ([ac0fb54](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/ac0fb54))

## [1.0.1](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/compare/v1.0.0...v1.0.1) (2019-12-27)


### Bug Fixes

* update some command line defaults ([7095854](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/7095854))

# 1.0.0 (2019-12-19)


### Features

* made into a module ([dd4efe3](https://gitlab.com/dreamer-labs/libraries/pypi-downloader/commit/dd4efe3))
